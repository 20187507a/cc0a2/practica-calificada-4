package com.uni.jmalpv.pc4;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

public class DeliveryActivity extends AppCompatActivity {

    String itemFoodName;

    Spinner spinnerNum;
    ArrayAdapter adapter;
    int numDelivery = 0;

    TextView textViewDelivery;
    EditText editTextDstName, editTextDstDirection;
    RadioButton radioButtonVisa, radioButtonCash;
    Button buttonDelivery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);

        itemFoodName = getIntent().getStringExtra("Item");

        textViewDelivery = findViewById(R.id.text_view_delivery);
        editTextDstName = findViewById(R.id.edit_text_name);
        editTextDstDirection = findViewById(R.id.edit_text_direction);
        radioButtonVisa = findViewById(R.id.radio_button_visa);
        radioButtonCash = findViewById(R.id.radio_button_cash);
        buttonDelivery = findViewById(R.id.button_delivery);

        textViewDelivery.setText(itemFoodName);

        spinnerNum = findViewById(R.id.spinner);

        adapter = ArrayAdapter.createFromResource(this, R.array.num_delivery, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerNum.setAdapter(adapter);

        spinnerNum.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    numDelivery = 1;
                }

                if (position == 1) {
                    numDelivery = 2;

                }
                if (position == 2) {
                    numDelivery = 3;
                }
                if (position == 3) {
                    numDelivery = 4;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        buttonDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!radioButtonVisa.isChecked() && !radioButtonCash.isChecked() || editTextDstName.getText().toString().isEmpty() || editTextDstDirection.getText().toString().isEmpty()) {
                    Snackbar.make(view, R.string.snack_bar_msg, Snackbar.LENGTH_LONG).show();
                    return;
                }

                Resources res = getResources();

                AlertDialog.Builder builder = new AlertDialog.Builder(DeliveryActivity.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(String.format(res.getString(R.string.delivery_accept), editTextDstName.getText(), editTextDstDirection.getText(), numDelivery));
                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.create().show();
            }
        });

    }
}