package com.uni.jmalpv.pc4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = findViewById(R.id.grid_view);
        fillArray();

        GridAdapter gridAdapter = new GridAdapter(this, text, image);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DeliveryActivity.class);
                intent.putExtra("Item", text.get(position));
                startActivity(intent);
                finish();
            }
        });
    }

    private void fillArray() {

        text.add("Pollo a la plancha");
        text.add("Aji de gallina");
        text.add("Ensalada de brocoli");
        text.add("Lentejas con pollo");
        text.add("Plato 5");
        text.add("Plato 6");
        text.add("Plato 7");
        text.add("Plato 8");

        image.add(R.drawable.a);
        image.add(R.drawable.b);
        image.add(R.drawable.c);
        image.add(R.drawable.d);
        image.add(R.drawable.a);
        image.add(R.drawable.a);
        image.add(R.drawable.a);
        image.add(R.drawable.a);
    }
}